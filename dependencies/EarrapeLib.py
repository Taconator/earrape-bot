from pydub import AudioSegment
import numpy as np
import os
import math
import youtube_dl
import time

'''

EarrapeLib.py
Created by Taconator

'''

YTUPLOAD = "C:\\users\\bunta\\Desktop\\Earrape\\bin\\youtube-upload.bat"


def bass_line_freq(track):
    sample_track = list(track)

    # c-value
    est_mean = np.mean(sample_track)

    # a-value
    est_std = 3 * np.std(sample_track) / (math.sqrt(2))

    bass_factor = int(round((est_std - est_mean) * 0.005))

    return bass_factor


def Earrape(FilePath : str, ExportPath : str, Amplification : int = 60, Format : str = 'mp3', Bitrate : str = '320k'):
    FileAS = AudioSegment.from_file(FilePath)
    FileAS = FileAS + Amplification
    FileAS.export(ExportPath, format='mp3', bitrate='320k')


def EarrapeChain_Start(FilePath : str, Amplification : int = 60):
    FileAS = AudioSegment.from_file(FilePath)
    return FileAS + Amplification


def EarrapeChain(FileAS : AudioSegment, Amplification : int = 60):
    return FileAS + Amplification


def BassBoost(FilePath : str, ExportPath : str, Attenuation : int = 30, Accentuation : int = 60):
    Sample = AudioSegment.from_file(FilePath)
    Filtered = Sample.low_pass_filter(100)
    Combined = (Sample - Attenuation).overlay(Filtered + Accentuation)
    Combined.export(ExportPath, format='mp3', bitrate='320k')


def BassBoostChain_Start(FilePath : str, Attenuation : int = 30, Accentuation : int = 60):
    Sample = AudioSegment.from_file(FilePath)
    Filtered = Sample.low_pass_filter(100)
    return (Sample - Attenuation).overlay(Filtered + Accentuation)


def BassBoostChain(FileAS : AudioSegment, Attenuation : int = 30, Accentuation : int = 60):
    Filtered = FileAS.low_pass_filter(150)
    return (FileAS - Attenuation).overlay(Filtered + Accentuation)


def FinishChain(FileAS : AudioSegment, ExportPath : str, Format : str = 'mp3', bitrate : str = '320k'):
    FileAS.export(ExportPath, format='mp3', bitrate='320k')


def DownloadAndEarrape(URL : str = "", Attentuation : int = 0, Accentuation : int = 75, Amplification : int = 60):
    if URL == "":
        return
    Name = time.time()
    ydl_opts = {
        'format' : 'bestaudio/best',
        'outtmpl' : str(Name).split('.')[0] + '.%(ext)s',
        'postprocessors'  : [{
            'key' : 'FFmpegExtractAudio',
            'preferredcodec' : 'mp3',
            'preferredquality' : '320',
        }],
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([URL])
    Chain = BassBoostChain_Start(f"{str(Name).split('.')[0]}.mp3", 20, 75)
    FinishChain(Chain, f"{str(Name).split('.')[0]}Earraped.mp3")
    os.remove(str(Name).split('.')[0] + ".mp3")
    return f"{str(Name).split('.')[0]}Earraped.mp3"


def CreateVideo(AudioPath : str = "", ImagePath : str = "clorox.png"):
    if AudioPath == "":
        return
    os.system(f'"C:\\Users\\bunta\Downloads\\ffmpeg-20180906-70a7087-win64-static\\bin\\ffmpeg.exe" -loop 1 -framerate 2 -i {ImagePath} -i {AudioPath} -c:v libx264 -threads 0 -preset medium -tune stillimage -crf 18 -c:a copy -shortest -pix_fmt yuv420p -y {AudioPath.split(".")[0]}Video.mkv')
    return f"{AudioPath.split('.')[0]}Video.mkv"


def UploadVideo(VideoPath : str = "", Title : str = "", Description : str = "", Category : str = "", Tags : str = "", Privacy : str = ""):
    if VideoPath == "" or Title == "" or Category == "":
        return
    os.system(f'{YTUPLOAD} --title "{Title}" --description "{Description}" --category "{Category}" --privacy "{Privacy}" {VideoPath}')