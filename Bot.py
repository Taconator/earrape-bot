import asyncio
import os
import time
from threading import Thread
from discord.ext import commands
import discord
import EarrapeLib


def get_prefix(bot, message):
    prefixes = ['!erp ']
    if not message.server:
        return '!erp '
    return commands.when_mentioned_or(*prefixes)(bot, message)


Bot = commands.Bot(command_prefix=get_prefix, description='Earrape Bot')
LogChannel = None
RequestsChannel = None

@Bot.event
async def on_ready():
    global LogChannel, RequestsChannel
    LogChannel = Bot.get_server("485744927980912661").get_channel("487839323090321408")
    RequestsChannel = Bot.get_server("485744927980912661").get_channel("488416360875950089")
    print(f'\n\n{Bot.user.name} ({Bot.user.id}) Ready\nVersion: {discord.__version__}\n')
    await Bot.change_presence(game=discord.Game(name='!erp'))
    print(f'{Bot.user.name} done initializing')


@Bot.group(name='request', pass_context=True)
async def Request(ctx):
    if ctx.invoked_subcommand is None:
        await Bot.say('**No request type entered** Command Usage:\n\n`!erp request automatic [youtube url] [artist] - [title]` for automatic requests (created by the bot)\n\n`!erp request manual [youtube url or song name and title]` for requests done manually')


@commands.cooldown(1, 60, commands.BucketType.user)
@Request.command(name='manual', pass_context=True)
async def Request_Manual(ctx):
    Args = ctx.message.content.split(' ', 3)
    if not len(Args) > 3:
        await Bot.say('**Failed to save your request.**\nDo `!erp request` for more information')
        return
    await Bot.send_message(RequestsChannel, f"**New Request**\n\n**Request:** {Args[2]}\n**Requester:** {ctx.message.author.name}\n**Request Type:** {Args[2].title()}")


@commands.cooldown(1, 60, commands.BucketType.user)
@Request.command(name='automatic', pass_context=True)
async def Request_Automatic(ctx):
    Args = ctx.message.content.split(' ', 4)
    if not len(Args) > 4:
        await Bot.say('**Failed to save your request.**\nDo `!erp request` for more information')
        return
    await Bot.send_message(RequestsChannel, f"Video-{Args[3]}\nVideo Title-{Args[4]}\nRequester-{ctx.message.author.name}\nRequest Type-{Args[2]}")
    await Bot.say('**Successfully added your request**\nNow you wait for it to be approved')


@Request.command(name='approve', pass_context=True)
async def Request_Approve(ctx):
    if ctx.message.author.id == '293827258748108801' or ctx.message.author.id == '157619512710135808':
        Args = ctx.message.content.split(' ', 3)
        if not len(Args) > 3:
            await Bot.say('**Failed to approve your request.**\nNeed to give the message ID of the request')
            return
        Message = await Bot.get_message(RequestsChannel, Args[3])
        Request = dict([Argument.split('-', 1) for Argument in [Line.strip() for Line in Message.content.splitlines()]])
        if Request['Request Type'] != 'automatic':
            await Bot.say('**This is a manual request so the bot will not do it**')
        ErpThread = Earrape(Request, asyncio.get_event_loop())
        ErpThread.start()
    else:
        await Bot.say('**You\'re not allowed to approve requests**\nDumb nigga, what you thinkin\' \'bout?')


async def LogMessage(Message : str):
    await Bot.send_message(LogChannel, Message)


class Earrape(Thread):
    def __init__(self, Request : dict, EventLoop):
        Thread.__init__(self)
        self.Request = Request
        self.Loop = EventLoop
    def run(self):
        Request = self.Request
        loop = self.Loop
        asyncio.ensure_future(LogMessage(f'**Beginning to process request** {Request["Video"]}'), loop=loop)
        Start = time.time()
        EarrapedAudio = EarrapeLib.DownloadAndEarrape(Request['Video'], Accentuation=120)
        EarrapedVideo = EarrapeLib.CreateVideo(EarrapedAudio)
        EarrapeLib.UploadVideo(EarrapedVideo, f'{Request["Video Title"]} [Chernobyl Edition]', 'Chernobyl Causer: Chernobyl Bot\\nJoin Our Discord: https://discord.me/earrape', 'Music', 'bass boosted, bass boost, earrape music, earrape bot, earrape, taconator, tacos, fros', 'public')
        os.remove(EarrapedAudio)
        os.remove(EarrapedVideo)
        asyncio.ensure_future(LogMessage(f"**Request finished {Request['Video']}**\nTook {round(time.time() - Start, 2)} seconds"), loop=loop)


Bot.run('Token', bot=True, reconnect=True)